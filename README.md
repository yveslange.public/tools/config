# Yves Lange Config

Here is my WSL2 configuration files. 

You can copy the file or create symbolic links from the
file of this repository.

**WARNING**: Be aware that this will override all your current settings. It is
your responsability to backup all your files (check the installation script).

## Configuration files

- bashrc
- inputrc
- nvim
- tmux
- zshrc

## Installation
Just run: 
`./install.sh`


### Fonts
On windows install the Nerd Fonts to have all unicode icons working with VIM
NerdTree.

Download and install the fonts from here:
https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/FiraCode.zip
