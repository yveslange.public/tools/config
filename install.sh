DIR=`pwd`

echo "Install dependencies"
sudo apt update
sudo apt -y upgrade
sudo apt -y install libfuse2 zsh tig

echo "Install Oh-My-ZSH"
if [ ! -d "${HOME}/.oh-my-zsh" ]; then
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

echo "Install configuration files"
mkdir -p ${HOME}/.config/
ln -sf --backup ${DIR}/bashrc ${HOME}/.bashrc
ln -sf --backup ${DIR}/zshrc ${HOME}/.zshrc
ln -sf --backup ${DIR}/inputrc ${HOME}/.inputrc
ln -sf --backup ${DIR}/npmrc ${HOME}/.npmrc
ln -sf --backup ${DIR}/tmux.conf ${HOME}/.tmux.conf
ln -sf --backup ${DIR}/gitconfig ${HOME}/.gitconfig
mkdir -p ${HOME}/.config/nvim
ln -sf --backup ${DIR}/nvim ${HOME}/.config/nvim/init.vim
ln -sf --backup ${DIR}/coc-settings.json ${HOME}/.config/nvim/coc-settings.json
mkdir -p ${HOME}/.config/lazygit/
ln -sf --backup ${DIR}/lazygit.yml ${HOME}/.config/lazygit/config.yml

echo "Install NVM"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
# Load nvm temporarily
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
nvm install node
nvm use node

echo "Install NeoVim"
curl --create-dirs -L -o "${HOME}/.neovim/nvim.appimage" -# https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod +x "${HOME}/.neovim/nvim.appimage"
sudo ln -fs "${HOME}/.neovim/nvim.appimage" /usr/bin/nvim
echo "Installing VIM-Plug for neovim"
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

echo "Installing plugins"
nvim +PlugInstall +CocInstall +qall
vi +PlugInstall +CocInstall +qall
vim +PlugInstall +CocInstall +qall

echo "Install lazygit"
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
sudo install lazygit /usr/local/bin
rm lazygit lazygit.tar.gz

echo "Install git delta"
GitDeltaVersion="0.16.5"
wget https://github.com/dandavison/delta/releases/download/${GitDeltaVersion}/git-delta_${GitDeltaVersion}_amd64.deb
sudo dpkg -i git-delta_${GitDeltaVersion}_amb64.deb
rm git-delta_${GitDeltaVersion}_amb64.deb


# Git
echo "Install git shortcuts"
git config --global pager.branch false
git config --global alias.st status
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.last 'log -1 HEAD'
git config --global alias.unstage 'reset HEAD --'

# Exit to force restart
exit
